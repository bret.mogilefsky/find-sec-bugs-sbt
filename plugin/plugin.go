package plugin

import (
	"os"

	"gitlab.com/gitlab-org/security-products/analyzers/common/plugin"
)

func Match(path string, info os.FileInfo) (bool, error) {
	if info.Name() == "build.sbt" {
		return true, nil
	}
	return false, nil
}

func init() {
	plugin.Register("find-sec-bugs-sbt", Match)
}
