package main

import (
	"fmt"
	"io"
	"os"
	"os/exec"
	"path/filepath"

	"github.com/urfave/cli"
)

const flagBuild = "build"

func analyzeFlags() []cli.Flag {
	return []cli.Flag{
		cli.BoolTFlag{
			Name:   flagBuild,
			EnvVar: "SAST_BUILD_PROJECT",
			Usage:  "Build Scala application. It's not needed if the code is already compiled.",
		},
	}
}

const (
	pathSbt         = "/usr/local/bin/sbt"
	pathJava        = "/usr/bin/java"
	pathFindSecBugs = "/findsecbugs-cli"
	pathPlugins     = pathFindSecBugs + "/lib/findsecbugs-plugin-1.7.1.jar"
	pathInclude     = pathFindSecBugs + "/include.xml"
	pathJarsList    = "/tmp/jars.list"
	pathOutput      = "/tmp/findSecurityBugs.xml"
	pathHome        = "/root"
)

func analyze(c *cli.Context, path string) (io.ReadCloser, error) {
	var setupCmd = func(cmd *exec.Cmd) *exec.Cmd {
		cmd.Env = os.Environ()
		cmd.Stdout = os.Stdout
		cmd.Stderr = os.Stderr
		cmd.Dir = path
		return cmd
	}

	// Build project. This will have the side-effect of downloading the JAR files.
	if c.BoolT(flagBuild) {
		// Use sbt wrapper when available
		binaryPath := "sbt"
		if _, err := os.Stat(binaryPath); err != nil {
			// Project has no wrapper for sbt
			binaryPath = pathSbt
		}
		if err := setupCmd(exec.Command("sbt", "compile")).Run(); err != nil {
			return nil, err
		}
	}

	// Build list of JARs
	if err := buildJarsList(); err != nil {
		return nil, err
	}

	// FSB CLI arguments
	// See https://github.com/find-sec-bugs/find-sec-bugs/wiki/CLI-Tutorial
	args := []string{
		"-cp",
		pathFindSecBugs + "/lib/*",
		"edu.umd.cs.findbugs.LaunchAppropriateUI",
		"-pluginList", pathPlugins,
		"-include", pathInclude,
		"-xml:withMessages",
		"-auxclasspathFromFile", pathJarsList,
		"-output", pathOutput,
	}

	// Append target directories. They contain the generated .class files.
	err := filepath.Walk(path, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}
		if info.IsDir() && info.Name() == "target" {
			args = append(args, path)
		}
		return nil
	})
	if err != nil {
		return nil, err
	}

	// Run FSB CLI
	if err := setupCmd(exec.Command(pathJava, args...)).Run(); err != nil {
		return nil, err
	}

	return os.Open(pathOutput)
}

func buildJarsList() error {
	f, err := os.OpenFile(pathJarsList, os.O_CREATE|os.O_WRONLY|os.O_TRUNC, 0644)
	if err != nil {
		return err
	}
	defer f.Close()

	dirs := []string{
		pathHome, // User cache
		"/usr/local/sbt/lib/local-preloaded",             // Sbt pre-defined
		"/usr/local/scala-" + os.Getenv("SCALA_VERSION"), // Scala pre-defined
	}

	for _, dir := range dirs {
		if err := filepath.Walk(dir, func(path string, info os.FileInfo, err error) error {
			if err != nil {
				return err
			}
			if !info.IsDir() && filepath.Ext(info.Name()) == ".jar" {
				if _, err := fmt.Fprintln(f, path); err != nil {
					return err
				}
			}
			return nil
		}); err != nil {
			return err
		}
	}

	return nil
}
