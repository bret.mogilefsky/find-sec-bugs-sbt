package main

import (
	"encoding/xml"
	"fmt"
	"io"
	"path/filepath"

	"gitlab.com/gitlab-org/security-products/analyzers/common/issue"
)

func convert(reader io.Reader, prependPath string) ([]issue.Issue, error) {
	var doc = struct {
		BugInstances []BugInstance `xml:"BugInstance"`
	}{}

	err := xml.NewDecoder(reader).Decode(&doc)
	if err != nil {
		return nil, err
	}

	issues := make([]issue.Issue, len(doc.BugInstances))
	for i, bug := range doc.BugInstances {
		issues[i] = issue.Issue{
			Category:    issue.CategorySast,
			Tool:        toolID,
			Name:        bug.ShortMessage,
			Message:     bug.ShortMessage,
			Description: bug.LongMessage, // Could be extracted from BugPattern/Details instead
			CompareKey:  bug.CompareKey(),
			Severity:    bug.Severity(),
			Confidence:  bug.Confidence(),
			// Solution: bug.Solution(), Need to parse BugPattern/Details to extract solution
			Location:    bug.Location(prependPath),
			Identifiers: bug.Identifiers(),
			// Links:    bug.Links(), Need to parse BugPattern/Details to extract links
		}
	}
	return issues, nil
}

// BugInstance is used to unmarshal BugInstance XML elements.
type BugInstance struct {
	Type         string `xml:"type,attr"`
	CWEID        int    `xml:"cweid,attr"`
	Rank         int    `xml:"rank,attr"`
	Priority     int    `xml:"priority,attr"`
	InstanceHash string `xml:"instanceHash,attr"`
	ShortMessage string `xml:"ShortMessage"`
	LongMessage  string `xml:"LongMessage"`
	Class        struct {
		Name string `xml:"classname,attr"`
	} `xml:"Class"`
	Method struct {
		Name string `xml:"name,attr"`
	} `xml:"Method"`
	SourceLine struct {
		Start      int    `xml:"start,attr"`
		End        int    `xml:"end,attr"`
		SourcePath string `xml:"sourcepath,attr"`
	} `xml:"SourceLine"`
}

// CompareKey returns a string used to establish whether two issues are the same.
func (bug BugInstance) CompareKey() string {
	if bug.InstanceHash == "" && bug.Type == "" {
		return ""
	}
	return bug.InstanceHash + ":" + bug.Type
}

// Severity returns the normalized severity of the bug instance.
// See https://github.com/spotbugs/spotbugs/blob/3.1.1/spotbugs/src/main/java/edu/umd/cs/findbugs/BugRankCategory.java#L32
func (bug BugInstance) Severity() issue.Level {
	switch bug.Rank {
	case 1, 2, 3, 4:
		return issue.LevelCritical
	case 5, 6, 7, 8, 9:
		return issue.LevelHigh
	case 10, 11, 12, 13, 14:
		return issue.LevelMedium
	case 15, 16, 17, 18, 19, 20:
		return issue.LevelLow
	}
	return issue.LevelUnknown
}

// Confidence returns the normalized confidence of the bug instance.
// See https://github.com/spotbugs/spotbugs/blob/3.1.1/spotbugs/src/main/java/edu/umd/cs/findbugs/Priorities.java
func (bug BugInstance) Confidence() issue.Level {
	switch bug.Priority {
	case 1:
		return issue.LevelHigh
	case 2:
		return issue.LevelMedium
	case 3:
		return issue.LevelLow
	case 4:
		return issue.LevelExperimental
	case 5:
		return issue.LevelIgnore
	}
	return issue.LevelUnknown
}

// Location returns a structured Location
func (bug BugInstance) Location(prependPath string) issue.Location {
	return issue.Location{
		File:      filepath.Join(prependPath, "src/main/scala", bug.SourceLine.SourcePath),
		LineStart: bug.SourceLine.Start,
		LineEnd:   bug.SourceLine.End,
		Class:     bug.Class.Name,
		Method:    bug.Method.Name,
	}
}

// Identifiers returns the normalized identifiers of the vulnerability.
func (bug BugInstance) Identifiers() []issue.Identifier {
	identifiers := []issue.Identifier{
		bug.FSBIdentifier(),
	}

	// Add CWE ID
	if bug.CWEID != 0 {
		identifiers = append(identifiers, issue.CWEIdentifier(bug.CWEID))
	}

	return identifiers
}

// FSBIdentifier returns a structured Identifier for a FSB bug Type
func (bug BugInstance) FSBIdentifier() issue.Identifier {
	return issue.Identifier{
		Type:  "find_sec_bugs_type",
		Name:  fmt.Sprintf("Find Security Bugs-%s", bug.Type),
		Value: bug.Type,
		URL:   fmt.Sprintf("https://find-sec-bugs.github.io/bugs.htm#%s", bug.Type),
	}
}
