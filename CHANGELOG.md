# find-sec-bugs Scala sbt analyzer changelog

GitLab find-sec-bugs Scala sbt analyzer follows versioning of GitLab (`MAJOR.MINOR` only) and generates a `MAJOR-MINOR-stable` [Docker image](https://gitlab.com/gitlab-org/security-products/analyzers/find-sec-bugs-gradle/container_registry).

These "stable" Docker images may be updated after release date, changes are added to the corresponding section bellow.

## 11-2-stable

## 11-1-stable
- Show command error output

## 11-0-stable
- initial release
